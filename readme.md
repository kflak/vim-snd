# Vim-snd

[WIP] plugin to combine the awesome power of the text editor (neo)vim with the awesome power of the sound editor [snd](https://ccrma.stanford.edu/software/snd/snd/snd.html). It is still very early days, the development will happen in fits and starts, and there is no guarantee that any of this will pan out.

